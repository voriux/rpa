<?php

namespace AppBundle\Controller;

use AppBundle\Repository\EventRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UiController
 * @package AppBundle\Controller
 * @Route("/ui")
 */
class UiController extends Controller
{
    /**
     * @Route("/events", name="app_ui_events")
     * @param Request $request
     * @return array
     * @Template()
     */
    public function eventsAction(Request $request)
    {
        /** @var EventRepository $eventRepository */
        $eventRepository = $this->get('app.repository.event');

        $events = $eventRepository->latest();
        return [
            'events' => $events
        ];
    }
}
