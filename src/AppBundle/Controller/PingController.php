<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Event;
use AppBundle\Repository\EventRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PingController extends Controller
{
    /**
     * @Route("/ping", name="app_ping")
     * @param Request $request
     * @return Response
     */
    public function pingAction(Request $request)
    {
        /** @var EventRepository $eventRepository */
        $eventRepository = $this->get('app.repository.event');

        if (!$request->query->has('name') || !$request->query->has('action')) {
            return new Response('Error. Required attributes are missing');
        }
        $parts = explode('-', $request->query->get('name'));

        $event = new Event();
        $event->setAction($request->query->get('action'));
        $event->setClientName($parts[0]);
        $event->setRobotType($parts[1]);
        $event->setRobotNumber($parts[2]);
        if ($request->query->has('body')) {
            $event->setBody($request->query->get('body'));
        }
        $eventRepository->save($event);
        return new Response('PONG');
    }
}
