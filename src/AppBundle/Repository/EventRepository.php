<?php
namespace AppBundle\Repository;

use AppBundle\Entity\Event;
use Doctrine\ORM\EntityRepository;

class EventRepository extends EntityRepository
{
    public function latest()
    {
        return $this->findBy([], ['time' => 'DESC'], 200);
    }

    public function save(Event $entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush();
        return $entity->getId() ? $entity->getId() : false;
    }

    public function persist(Event $entity)
    {
        $this->_em->persist($entity);
    }

    public function flush(Event $entity)
    {
        $this->_em->persist($entity);
    }
}
