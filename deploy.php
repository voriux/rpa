<?php
/*
 * This file has been generated automatically.
 * Please change the configuration for correct use deploy.
 */

require 'recipe/symfony3.php';

// Set configurations
set('repository', getenv('CI_GITHOST_URL'));
//set('shared_files', ['app/config/parameters.yml']);
//set('shared_dirs', ['app/logs']);
//set('writable_dirs', ['app/cache', 'app/logs']);

// Configure servers
//server('production', 'prod.domain.com')
//    ->user('username')
//    ->password()
//    ->env('deploy_path', '/var/www/prod.domain.com');
serverList('app/config/servers.yml');
//server(getenv('CI_ENVIRONMENT_NAME'), getenv('DEPLOY_SERVER'))
//    ->user('username')
//    ->identityFile()
//    ->env('deploy_path', getenv('DEPLOY_PATH').'/'.getenv('CI_PROJECT_NAME'));

/**
 * Attention: This command is only for for example. Please follow your own migrate strategy.
 * Attention: Commented by default.
 * Migrate database before symlink new release.
 */

// before('deploy:symlink', 'database:migrate');
